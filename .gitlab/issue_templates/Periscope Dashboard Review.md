## Periscope Dashboard Checklist 

<!--
Please complete all items. Ask questions in #data
--->

**Dashboard Link**: 
`WIP:` should be in the title and it should be in the `WIP` topic

**Original Issue Link**:

**Editor Slack Handle**: @`handle`

**Requested review time**:
  - [ ] < 21 days (next milestone)
  - [ ] < 7 days (current milestone)
  - [ ] < 2 days (only for urgent needs)

If urgent, please detail why.

### Submitter Checklist

* Housekeeping
  - [ ] Assigned to a member of the data team
  - [ ] Allocated to milestone per review time request
  - [ ] Labels and Points Allocated

* Review Items
   * [ ] SQL formatted using automatic formatter
   * [ ] Python / R reviewed for content, formatting, and necessity 
   * [ ] Filters, if relevant
   * [ ] Current month (in-progress) numbers and historical numbers are in separate charts
   * [ ] Drill Down Linked, if relevant
   * [ ] Overview/KPI/Top Level Metrics cross-linked
   * [ ] Section Label before more granular metrics
   * [ ] Topics added
   * [ ] Permissions reviewed
   * [ ] Viz Titles changed to Autofit, if relevant
   * [ ] Axes labeled, if relevant
   * [ ] Numbers (Currencies, Percents, Decimal Places, etc) cleaned, if relevant
   * [ ] Chart description for each chart, linking to Metrics definitions where possible
   * [ ] Legend is clear
   * [ ] Text Tile for "What am I looking at?" and more detailed information, leveraging hyperlinks instead of URLs
   * [ ] MR created to add to Periscope Directory

### Reviewer Checklist
* Review Items
   * [ ] SQL formatted using automatic formatter
   * [ ] Python / R reviewed for content, formatting, and necessity 
   * [ ] Filters, if relevant
   * [ ] Current month (in-progress) numbers and historical numbers are in separate charts
   * [ ] Drill Down Linked, if relevant
   * [ ] Overview/KPI/Top Level Metrics cross-linked
   * [ ] Section Label before more granular metrics
   * [ ] Topics added
   * [ ] Permissions reviewed
   * [ ] Viz Titles changed to Autofit, if relevant
   * [ ] Axes labeled, if relevant
   * [ ] Numbers (Currencies, Percents, Decimal Places, etc) cleaned, if relevant
   * [ ] Chart description for each chart, linking to Metrics definitions where possible
   * [ ] Legend is clear
   * [ ] Merged into Periscope Directory
   * [ ] Remove `WIP:` from title
   * [ ] Remove from `WIP` topic
   * [ ] Add approval badge
   * [ ] Close this issue

/label ~Reporting ~Periscope ~Review