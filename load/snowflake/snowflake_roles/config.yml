version: "1.0"

# Databases
databases:
    - analytics:
        shared: no

    - raw:
        shared: no

    - testing_db:
        shared: no

    - snowflake:
        shared: yes


# Roles
roles:
    ## Admin Roles
    - accountadmin:
        warehouses:
            - loading
        member_of:
            - sysadmin

    - securityadmin:
        warehouses:
            - loading
        member_of:
            - sysadmin

    - sysadmin:
        warehouses:
            - loading
        privileges:
            databases:
                read:
                    - raw
                    - analytics
                write:
                    - raw
                    - analytics
            schemas:
                read:
                    - raw.*
                    - analytics.*
                write:
                    - raw.*
                    - analytics.*
            tables:
                read:
                    - raw.*.*
                    - analytics.*.*
                write:
                    - raw.*.*
                    - analytics.*.*

    ## Regular Roles
    - engineer:
        member_of:
            - sysadmin
        warehouses:
            - loading
        privileges:
            databases:
                read:
                    - raw
                    - analytics
                write:
                    - raw
                    - analytics
            schemas:
                read:
                    - raw.*
                    - analytics.*
                write:
                    - raw.*
                    - analytics.*
            tables:
                read:
                    - raw.*.*
                    - analytics.*.*
                write:
                    - raw.*.*
                    - analytics.*.*

    - loader:
        warehouses:
            - loading
        owns:
            schemas:
                - raw.*
                - testing_db.*
            tables:
                - raw.*.*
                - testing_db.*.*
        privileges:
            databases:
                read:
                    - raw
                    - testing_db
                write:
                    - raw
                    - testing_db
            schemas:
                read:
                    - raw.*
                    - testing_db.*
                write:
                    - raw.*
                    - testing_db.*
            tables:
                read:
                    - raw.*.*
                    - testing_db.*.*
                write:
                    - raw.*.*
                    - testing_db.*.*

    - product_manager:
        warehouse:
            - reporting
        privileges:
            databases:
                read:
                    - raw
            schemas:
                read:
                    - raw.ci_stats_db
                    - raw.customers_db
                    - raw.gitlab_dotcom
                    - raw.gitlab_profiler_db
                    - raw.license_db
                    - raw.snowplow
                    - raw.version_db
            tables:
                read:
                    - raw.ci_stats_db.*
                    - raw.customers_db.*
                    - raw.gitlab_dotcom.*
                    - raw.gitlab_profiler_db.*
                    - raw.license_db.*
                    - raw.snowplow.*
                    - raw.version_db.*

    - reporter:
        warehouses:
            - reporting
        privileges:
            databases:
                read:
                    - analytics
                    - snowflake
            schemas:
                read:
                    - analytics.analytics
                    - analytics.analytics_meta
                    - snowflake.account_usage

            tables:
                read:
                    - analytics.analytics.*
                    - analytics.analytics_meta.*
                    - snowflake.account_usage.*

    - reporter_sensitive: #Right now this matches the reporter role, until we roll out the sensitive schema 
        warehouses:
            - reporting
        privileges:
            databases:
                read:
                    - analytics
                    - snowflake
            schemas:
                read:
                    - analytics.analytics ##TODO:add sensitive schema
                    - analytics.analytics_meta
                    - snowflake.account_usage

            tables:
                read:
                    - analytics.analytics.*
                    - analytics.analytics_meta.*
                    - snowflake.account_usage.*

    - transformer:
        warehouses:
            - analyst_xl
            - analyst_xs
            - merge_request_xl
            - merge_request_xs
            - transforming
            - transforming_s
            - transforming_xl
            - transforming_xs
        owns:
            schemas:
                - analytics.*
            tables:
                - analytics.*.*
        privileges:
            databases:
                read:
                    - raw
                    - analytics
                write:
                    - analytics
            schemas:
                read:
                    - raw.*
                write:
                    - analytics.*
            tables:
                read:
                    - raw.*.*
                write:
                    - analytics.*.*

    # User Roles
    - acarella:
        warehouses:
            - reporting
        owns:
            schemas:
                - analytics.acarella_scratch

    - amrvaljevich:
        warehouses:
            - reporting
        owns:
            schemas:
                - analytics.amrvaljevich_scratch
                - analytics.amrvaljevich_scratch_meta

    - crichards:
        warehouses:
            - reporting
        owns:
            schemas:
                - analytics.crichards_scratch
                - analytics.crichards_scratch_meta

    - dtang:
        warehouses:
            - reporting
        owns:
            schemas:
                - analytics.dtang_scratch

    - eburke:
        warehouses:
            - reporting
        owns:
            schemas:
                - analytics.emilie_scratch
                - analytics.emilie_scratch_meta

    - eeastwood:
        warehouses:
            - reporting
        owns:
            schemas:
                - analytics.eeastwood_scratch
        privileges:
            databases:
                read:
                    - raw
            schemas:
                read:
                    - raw.gitter
                write:
                    - raw.gitter
            tables:
                read:
                    - raw.gitter.*
                write:
                    - raw.gitter.*

    - ekastelein:
        warehouses:
            - reporting
        owns:
            schemas:
                - analytics.ekastelein_scratch
                - analytics.ekastelein_scratch_meta

    - gitlab-ci:
        warehouses:
            - reporting

    - iroussos:
        warehouses:
            - reporting
        owns:
            schemas:
                - analytics.iroussos_scratch

    - jsalazar:
        warehouses:
            - reporting
        owns:
            schemas:
                - analytics.jsalazar_scratch
    
    - jurbanc:
        warehouses:
            - reporting
        owns:
            schemas:
                - analytics.jurbanc_scratch              

    - periscope:
        warehouses: 
            - reporting
    
    - periscope_sensitive:
        warehouses:
            - reporting

    - redash:
        warehouses: 
            - reporting  

    - ssichak:
        warehouses:
            - reporting
        owns:
            schemas:
                - analytics.ssichak_scratch

    - target_snowflake:
        warehouses:
            - reporting
        owns:
            schemas:
                - testing_db.target_snowflake

    - tlapiana:
        warehouses:
            - reporting
        owns:
            schemas:
                - analytics.tlapiana_scratch

    - tmurphy:
        warehouses:
            - reporting
        owns:
            schemas:
                - analytics.tmurphy_scratch
                - analytics.tmurphy_scratch_meta
    
    - valexieva:
        warehouses:
            - reporting
        owns:
            schemas:
                - analytics.valexieva_scratch
        privileges:
            databases:
                read:
                    - raw
            schemas:
                read:
                    - raw.gitlab_dotcom
                write:
                    - raw.gitlab_dotcom
            tables:
                read:
                    - raw.gitlab_dotcom.*
                write:
                    - raw.gitlab_dotcom.*

    - wwright:
        warehouses:
            - reporting
        owns:
            schemas:
                - analytics.wwright_scratch

# Users
users:
    - acarella:
        can_login: yes
        member_of:
            - securityadmin
            - sysadmin
            - acarella

    - amrvaljevich:
        can_login: yes
        member_of:
            - sysadmin
            - transformer
            - amrvaljevich        

    - crichards:
        can_login: yes
        member_of:
            - sysadmin
            - transformer
            - crichards 

    - dtang:
        can_login: yes
        member_of:
            - reporter
            - dtang

    - eburke:
        can_login: yes
        member_of:
            - sysadmin
            - transformer
            - eburke
           
    - eeastwood:
        can_login: yes
        member_of:
            - reporter
            - eeastwood

    - ekastelein:
        can_login: yes
        member_of:
            - sysadmin
            - transformer
            - eburke

    - gitlab-ci:
        can_login: yes
        member_of:
            - loader
            - reporter
            - sysadmin
            - transformer
            - gitlab-ci

    - iroussos:
        can_login: yes
        member_of:
            - engineer
            - loader
            - transformer
            - sysadmin
            - iroussos
    
    - jsalazar:
        can_login: yes
        member_of:
            - securityadmin
            - sysadmin
            - jsalazar

    - jurbanc:
        can_login: yes
        member_of:
            - securityadmin
            - sysadmin
            - jurbanc
            
    - periscope:
        can_login: yes
        member_of:
            - reporter
            - periscope

    - periscope_sensitive:
        can_login: yes
        member_of:
            - reporter_sensitive
            - periscope_sensitive            

    - redash:
        can_login: yes
        member_of:
            - reporter
            - redash

    - ssichak:
        can_login: yes
        member_of:
            - securityadmin
            - sysadmin
            - ssichak

    - target_snowflake:
        can_login: yes
        member_of:
            - loader
            - target_snowflake

    - tlapiana:
        can_login: yes
        member_of:
            - engineer
            - loader
            - transformer
            - sysadmin
            - tlapiana

    - tmurphy:
        can_login: yes
        member_of:
            - accountadmin
            - engineer
            - sysadmin
            - transformer
            - tmurphy

    - valexieva:
        can_login: yes
        member_of:
            - product_manager
            - reporter
            - valexieva

    - wwright:
        can_login: yes
        member_of:
            - engineer
            - transformer
            - wwright

# Warehouses
warehouses:
    - analyst_xl:
        size: x-large
    
    - analyst_xs:
        size: x-small

    - engineer_xl:
        size: x-large
    
    - engineer_xs:
        size: x-small

    - loading:
        size: x-small

    - merge_request_xl:
        size: x-large
    
    - merge_request_xs:
        size: x-small

    - reporting:
        size: x-small

    - transforming:
        size: x-large

    - transforming_s:
        size: small

    - transforming_l:
        size: large

    - transforming_xl:
        size: x-large
    
    - transforming_xs:
        size: x-small
        